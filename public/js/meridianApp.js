(function() {

    /*
     * This file defines the entire frontend functionality.
     * 
     * The GridController is the main controller of the application (the demo was originally
     * going to focus on the data grid, hence the name of the controller)
     *
     * The DataService fetches the dummy JSON data from the NodeJS server.
     *
     * The WebsocketService angular service handles client side websocket functionality
     * and the reconnection of a dropped server connection.
     */

    'use strict';

    var meridianApp = angular.module('meridianApp', ['ui.grid']);

    meridianApp.controller('GridController', ['$scope', 'DataService', 'WebsocketService', function($scope, DataService, WebsocketService) {
        $scope.gridOptions = {
            enableFiltering: true,
            infiniteScrollUp: true,
            infiniteScrollDown: true
        };
        $scope.message = '';

        // the WebsocketService broadcasts events from the $rootScope
        $scope.$on('Message', function(event, message) {
            console.log(message.data);
        });

        $scope.$on('Connected', function() {
            console.log('connected');
        });

        $scope.$on('Disconnected', function() {
            console.log('disconnected');
        });

        this.sendMessage = function() {
            WebsocketService.send($scope.message);
            $scope.message = '';
        };

        WebsocketService.connect({server:'http://localhost:3001/wsserver', retryTimeout:10000});

        DataService
                .loadData()
                .then(function(data) {
                    $scope.gridOptions.data = data;    
                });

    }]);

    // service loading dummy data from the backend nodejs application
    meridianApp.service('DataService', ['$http', function($http) {

        function handleSuccess(response) {
            return response.data;
        }

        function handleError(response) {
            return $q.reject('Error while fetching data');
        }

        function loadData() {
            var request = $http({
                method: 'GET',
                url: '/api'
            });
            return request.then(handleSuccess, handleError);
        }

        return {
            loadData: loadData
        };

    }]);

    // service to handle the websocket connection to the server
    meridianApp.service('WebsocketService', ['$rootScope', function($rootScope) {
        var self = this;

        self.connected = false;
        self.messageCache = [];

        self.connect = function(opts) {
            var socket = new SockJS(opts.server);
            // broadcast socket events through the $rootScope
            socket.onopen = function() {
                $rootScope.$broadcast('Connected');
                self.connected = true;
                sendCachedMessages(socket, self.messageCache, function() {self.messageCache = [];});
            };
            socket.onmessage = function(message) {
                $rootScope.$broadcast('Message', message);
            };
            socket.onclose = function(event) {
                if (event.wasClean) { // server requested that the connection be closed
                    $rootScope.$broadcast('ConnectionClosed');
                }
                else { // the connection was closed unexpectedly
                    // the long wait is intentional. it gives you time to cache some messages to test with.
                    $rootScope.$broadcast('Disconnected');
                    self.connected = false;
                    setTimeout(function() {
                        self.connect(opts);
                    }, opts.retryTimeout);
                }
            };
            self.socket = socket;       
        };
        
        self.send = function(message) {
            if (self.connected) {
                self.socket.send(message);
            }
            else {
                self.messageCache.push(message);
            }
        };

        function sendCachedMessages(socket, cache, callback) {
            if (cache.length === 0) {
                return;
            }
            // we delay resend to give other clients 
            // the chance to reconnect
            setTimeout(function() {
                for (var i = 0; i < cache.length; i++) {
                    var message = cache[i];
                    socket.send(message);
                }
                callback();
            }, 2000);
        }
    }]);

})();