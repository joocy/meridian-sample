(function() {

    /*
     * This module handles the server-side portion of the websocket connection. The module
     * will simply accept websocket connections from clients and relay any message sent
     * from any client connection to all other client connections.
     *
     * The module exports the configured server instance. It is up to the caller to start
     * the websocket server accepting connections by calling the listen function on the 
     * exported server instance.
     */

    'use strict';

    var http = require('http');
    var sockjs = require('sockjs');

    var connections = [];

    var socketServer = sockjs.createServer({sockjs_url: 'http://cdn.jsdelivr.net/sockjs/0.3.4/sock.min.js'});
    socketServer.on('connection', function(connection) {
        console.log('New client connection');
        connections.push(connection);
        connection.on('data', function(message) {
            console.log('message recieved:', message);
            sendMessage(connection, message);
        });
        connection.on('close', function() {
            console.log('Client closed connection');
            var connectionIdx = connections.indexOf(connection);
            connections.splice(connectionIdx, 1);
        });
    });

    function sendMessage(connection, message) {
        for (var i = 0; i < connections.length; i++) {
            var nextConnection = connections[i];
            if (nextConnection !== connection) {
                nextConnection.write(message);
            }
        }
    }

    var server = http.createServer();
    socketServer.installHandlers(server, {prefix:'/wsserver'});

    module.exports = server;

})();