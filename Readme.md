# Sample project for Meridian

## Purpose

This project is intended to show one solution to the issue of unreliable websocket connections. This project is not intended to be exhaustive in its coverage, nor particuarly functionally interesting. It is the result of a single day's work and, as such, could never be the much more than a simple example of what could be done with a little more time and thought.

The project uses SockJS for the websocket connection and manages reconnection when the server connection drops.

## Running the application

Having cloned the project repository, run ```npm install``` and ```bower install``` in the project folder. This will fetch all project dependencies. The project is managed by Gulp and is setup to be run as a livereload server. With Gulp installed, simply run the ```gulp``` command from the project folder. The server will start, by default, on port 3000 of your local machine.

## Using the application

Once you have the server running, the best way to see the websocket functionality is to load the application into two browser windows. Simple navigate to http://localhost:3000 in two (or more) browser windows. Open the Javascript console on all browsers to view output. 

You will see a table of data below which is an input and button. Enter a message into the input and click the button. The message will be sent to all other browsers. While keeping the browsers open, shut down the server by simply pressing ```Ctrl-C``` in the terminal used to start the server.

All browsers will be alerted that the server has gone down. By default, each browser will wait for 10 seconds before reconnecting. You can continue to send messages during this time. The browser will cache them and automatically send them when it reconnects to the server.


## How it's done

The entire AngularJS application is defined in the ```public/js/meridianApp.js``` file. The file contains four components:

- ```meridianApp``` is an angular module
- ```GridController``` is the single controller used on the page (the demo was originally going to focus more on the grid itself)
- ```DataService``` is a simple service to fetch the dummy JSON data from the NodeJS server
- ```WebsocketService``` encapsulates all the websocket functionality

WebsocketService is injected as a dependency into the GridController and exposes two functions: ```connect``` and ```send```. The connect function connects to the backend websocket server (implemented using SockJS in the NodeJS server) and the send function sends the provided message to all other connections. The WebsocketService will broadcast all socket events from the $rootScope so that controllers need only add event lisnteners on their scopes to react to socket events.

The reconnect is done in the ```onclose``` handler of the socket. This handler gets called whenever the server closes the connection. The browser will wait for the specified timeout before trying to connect. Instead of trying ad infinitum, it would be better, in production, to limit the retries or have a sliding timeout or a combination of both.

## Improvements

This demo doesn't cover any pubsub functionality. Its focus was on the single issue of unreliable websocket connections. It would be possible to use the WebsocketService (or something like it) as a message router on the client side, or push that logic up to the server. It may still be the case (and, if so, the recommended way) that you could find a use for a higher level library like socket.io or Faye, rather than having to code this functionality yourselves.

It may be that stomp.js provides similar functionality in the pubsub realm as sockjs does in the websocket, but this was not investigated.