(function() {

    'use strict';

    var express = require('express');
    var router = express.Router();

    var incomingData = require('./incomingData');

    router.get('/', function(req, res) {
        res.set('Content-Type', 'application/json');
        res.status(200).send(incomingData);
    });

    module.exports = router;

})();